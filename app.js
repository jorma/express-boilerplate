process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const http = require('http');
const config = require('./src/config/environment');
const db = require('./src/utils/db');
const logger = require('./src/utils/logger');

if (config.opbeat.enabled) {
  require('./src/utils/opbeat'); // eslint-disable-line import/no-unassigned-import
}

const express = require('./src');

const server = http.createServer(express);

async function initialize() {
  try {
    await db.initMongo(`mongodb://${config.mongo.host}/${config.mongo.database}`, { useMongoClient: true });
    server.listen(config.port, () => {
      logger.info(`Server listening on ${config.port}, in ${config.env} mode`);
    });
  } catch (err) {
    logger.error(`MongoDB Server is down...\n`);
  }
}

initialize();
