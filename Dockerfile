FROM node:8.9.1

EXPOSE 9003

RUN mkdir -p /opt/express-boilerplate
WORKDIR /opt/express-boilerplate

COPY package.json /opt/express-boilerplate
RUN npm --no-color install --production

COPY . /opt/express-boilerplate

CMD ["npm", "run", "start:prod"];
