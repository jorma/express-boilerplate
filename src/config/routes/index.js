const index = require('../../routes/index');
const api = require('../../routes/api');

module.exports = [
  {
    path: '',
    router: index,
    childs: [
      {
        path: '/api',
        router: api
      }
    ]
  }
];
