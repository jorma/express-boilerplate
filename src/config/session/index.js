const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const config = require('../environment');

function addSessionToApp(app) {
  const store = {
    store: new RedisStore({
      host: config.redis.connection.host,
      port: config.redis.connection.port,
      prefix: 'express-boilerplate:sess:'
    })
  };

  const sessionConfig = Object.assign(config.session, store);

  app.use(session(sessionConfig));
}

module.exports = addSessionToApp;
