const path = require('path');
const _ = require('lodash');

const envConfig = require(`./${process.env.NODE_ENV || 'development'}.js`);

const rootPath = path.normalize(`${__dirname}/../..`);

const all = {
  env: process.env.NODE_ENV,
  root: rootPath,
  port: process.env.BACK_PORT || 9003,
  opbeat: {
    enabled: false
  },
  mongo: {
    host: 'mongodb',
    database: 'express-boilerplate-dev'
  },
  redis: {
    enabled: true,
    renew: true,
    connection: {
      host: 'redis',
      port: 6379,
      password: '',
      db: 0,
      keyPrefix: 'express-boilerplate:'
    }
  },
  session: {
    cookie: {
      path: '/',
      httpOnly: false,
      secure: false,
      maxAge: null
    },
    secret: 'express-boilerplate',
    resave: false,
    rolling: true,
    saveUninitialized: true
  }
};

const config = _.merge(all, envConfig);
module.exports = config;
