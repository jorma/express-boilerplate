module.exports = {
  logger: {
    logLevel: 'debug', // { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
    handleExceptions: false,
    defaultLogger: {
      console: {
        enable: false
      },
      file: {
        enable: false,
        fileName: 'logs.log'
      }
    },
    httpLogger: {
      console: {
        enable: false
      },
      file: {
        enable: false,
        fileName: 'http-logs.log'
      }
    }
  },
  mongo: {
    host: 'localhost',
    database: 'express-boilerplate-dev'
  },
  redis: {
    enabled: true,
    renew: true,
    connection: {
      host: 'localhost',
      port: 6379,
      password: '',
      db: 0,
      keyPrefix: 'express-boilerplate:'
    }
  }
};
