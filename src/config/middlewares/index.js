const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const celebrate = require('celebrate');
const {
  opbeat
} = require('../environment');
const accessControl = require('./access-control');

function setupMiddlewares(app) {
  app.use(helmet());
  app.use(celebrate.errors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(accessControl);
  app.use(morgan('common'));

  if (opbeat.enabled) {
    const opbeat = require('../../utils/opbeat');
    app.use(opbeat.middleware.express());
  }
}

module.exports = setupMiddlewares;
