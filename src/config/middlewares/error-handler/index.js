const errorHandler = require('errorhandler');
const { env } = require('../../environment');
const logger = require('../../../utils/logger');

function getErrorHandler() {
  if (env === 'production') {
    const productionHandler = require('./production-handler');
    return productionHandler;
  }

  return errorHandler({
    log: (error, str) => {
      logger.error(str);
    }
  });
}

module.exports = getErrorHandler();
