const fs = require('fs');
const path = require('path');
const changeCase = require('change-case');
const _ = require('lodash');

const exportObject = { };

try {
  const dirList = fs.readdirSync(path.join(__dirname, ('/src/models')));
  for (const item of dirList) {
    if (!item.startsWith('.')) {
      const exportName = _.upperFirst(changeCase.camelCase(item));
      exportObject[exportName] = require(`./src/models/${item}`);
    }
  }

  exportObject.initDb = require('./src/utils/db').init;
} catch (err) {
  console.error(err);
}

module.exports = exportObject;
