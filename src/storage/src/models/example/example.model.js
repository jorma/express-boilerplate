const mongoose = require('mongoose');
const ExampleSchema = require('./example.schema');

const Example = mongoose.model('Example', ExampleSchema);

module.exports = Example;
