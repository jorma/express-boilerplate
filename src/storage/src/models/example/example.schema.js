const mongoose = require('mongoose');
const exampleDef = require('./example.def');

const Schema = mongoose.Schema;
const ExampleSchema = new Schema(exampleDef, { timestamps: true });
module.exports = ExampleSchema;
