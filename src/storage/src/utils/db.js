const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
exports.init = (urlConnection, options) => {
  return mongoose.connect(urlConnection, options);
};
