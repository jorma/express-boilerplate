const express = require('express');
const celebrate = require('celebrate');
const routesConfig = require('./config/routes');
const middlewares = [
  require('./config/session'),
  require('./config/middlewares')
];
const errorMiddlewares = [
  require('./config/middlewares/error-handler')
];

const { makeRoutes } = require('./utils/helpers/routes.scaffolding');

const app = express();
app.enable('trust proxy');
middlewares.forEach((middleware) => middleware(app));
makeRoutes(routesConfig, app);
app.use(celebrate.errors());
errorMiddlewares.forEach((middleware) => app.use(middleware));
app.all('*', (req, res, next) => res.status(404).send());

module.exports = app;
