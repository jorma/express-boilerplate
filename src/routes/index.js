const express = require('express');
const am = require('../config/middlewares/async-middleware');
const HomeController = require('../controllers/home/home.controller');

const router = new express.Router();
router.get('/', HomeController.index.bind(HomeController));
router.get('/robots.txt', am(HomeController.getRobots.bind(HomeController)));

module.exports = router;
