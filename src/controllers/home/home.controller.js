const packageJson = require('../../../package.json');
const RedisClient = require('../../utils/redis/redis.class');
const Controller = require('../template.class');

class HomeController extends Controller {
  constructor() {
    super();
    return this;
  }

  index(req, res, next) {
    const { httpStatus } = this;

    const { name, version } = packageJson;
    const returnObj = { name, version };
    return res.status(httpStatus.OK).send(returnObj);
  }

  async getRobots(req, res, next) {
    const { httpStatus } = this;
    res.type('text/plain');
    const rc = await RedisClient.cacheFn('robots', () => {
      return Promise.resolve('User-agent: *\nDisallow: /');
    });

    return res.status(httpStatus.OK).send(rc);
  }
}

module.exports = new HomeController();
