const HttpStatus = require('http-status-codes');

class Controller {
  constructor() {
    this.httpStatus = HttpStatus;
  }
}

module.exports = Controller;
