const { initDb } = require('../../storage');
const logger = require('../logger');

exports.initMongo = (urlConnection, options) => {
  logger.info(`Connection to MongoDB`);

  return initDb(urlConnection, options);
};
