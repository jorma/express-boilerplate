const test = require('ava');
const sinon = require('sinon');
const mockery = require('mockery');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('initMongo is calling initDb', function * (t) {
  const initDbStub = sinon.stub()
    .withArgs('mongodb://test', {})
    .resolves();

  const storageMock = {
    initDb: initDbStub
  };

  const loggerMock = {
    info: () => { }
  };

  mockery.registerMock('../../storage', storageMock);
  mockery.registerMock('../logger', loggerMock);

  const util = require('../');
  yield util.initMongo('mongodb://test', { });

  t.truthy(initDbStub.calledOnce);
  t.truthy(initDbStub.calledWithExactly('mongodb://test', { }));
});
