exports.makeRoutes = (routes, app, prefix) => {
  routes.forEach(({ path, router, childs }) => {
    if (prefix) {
      path = `${prefix}${path}`;
    }

    app.use(path, router);

    if (childs) {
      return exports.makeRoutes(childs, app, path);
    }
  });
};
