const test = require('ava');
const mockery = require('mockery');
const sinon = require('sinon');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('opbeat is called with the expected parameter', (t) => {
  const environmentMock = {
    opbeat: {
      enabled: false,
      organizationId: 'test',
      secretToken: 'secretTest'
    }
  };

  const startStub = sinon.stub().resolves();

  const opbeatMock = {
    start: startStub
  };

  mockery.registerMock('../../config/environment', environmentMock);
  mockery.registerMock('opbeat', opbeatMock);
  require('../'); // eslint-disable-line import/no-unassigned-import
  t.truthy(startStub.calledOnce);
});
