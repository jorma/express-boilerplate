exports.renameKey = (obj, oldKey, newKey) => {
  obj[newKey] = obj[oldKey];
  delete obj[oldKey];

  return obj;
};

exports.sortObject = (obj, sortWith) => {
  return Object.keys(obj).sort(sortWith)
    .reduce((total, key) => {
      total[key] = obj[key];

      return total;
    }, Object.create(null));
};
