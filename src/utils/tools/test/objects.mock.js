module.exports = {
  renameKey: {
    oldObj: {
      test: 'test',
      noChange: 'no-change'
    },
    expectedObj: {
      t3st: 'test',
      noChange: 'no-change'
    }
  },
  sortObject: {
    obj: {
      c: '2',
      b: '3',
      a: '1'
    },
    sortExpectedObj: {
      a: '1',
      b: '3',
      c: '2'
    },
    sortFnExpectedObj: {
      a: '1',
      c: '2',
      b: '3'
    }
  }
};
