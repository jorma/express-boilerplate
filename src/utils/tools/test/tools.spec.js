const test = require('ava');
const {
  renameKey: {
    oldObj,
    expectedObj
  },
  sortObject: {
    obj,
    sortExpectedObj,
    sortFnExpectedObj
  }
} = require('./objects.mock');

test('renameKey is working as expected', (t) => {
  const { renameKey } = require('../');

  const newObj = renameKey(oldObj, 'test', 't3st');
  t.deepEqual(newObj, expectedObj);
});

test('sortObject without a function works as expected', (t) => {
  const { sortObject } = require('../');

  const newObj = sortObject(obj);
  t.deepEqual(newObj, sortExpectedObj);
});

test('sortObject with a function works as expected', (t) => {
  const { sortObject } = require('../');
  const newObj = sortObject(obj, (a, b) => {
    if (obj[a] < obj[b]) {
      return -1;
    }

    if (obj[a] > obj[b]) {
      return 1;
    }

    return 0;
  });

  t.deepEqual(newObj, sortFnExpectedObj);
});
