const test = require('ava');
const mockery = require('mockery');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('consoleTransport get the correct parameters', (t) => {
  const environmentMock = {
    logLevel: 'testLevel'
  };

  mockery.registerMock('../../config/environment', environmentMock);
  const util = require('../');

  t.is(util.level, environmentMock.logLevel);
});

