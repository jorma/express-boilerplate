const sinon = require('sinon');

module.exports = {
  lean: sinon.stub(),
  findOne: sinon.stub(),
  exec: sinon.stub(),
  find: sinon.stub(),
  create: sinon.stub()
};
