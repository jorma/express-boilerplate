const test = require('ava');
const mockery = require('mockery');
const sinon = require('sinon');

mockery.enable({
  useCleanCache: true,
  warnOnUnregistered: false,
  warnOnReplace: false
});

test.beforeEach(() => {
  mockery.resetCache();
});

test.after(() => {
  mockery.deregisterAll();
});

test('when instantiates the repo has a model', (t) => {
  const modelMock = require('./model.mock');
  const BaseRepository = require('../template.class');
  const baseRepository = new BaseRepository(modelMock);
  t.deepEqual(baseRepository.model, modelMock);
});

test('when instantiates the repo no methods of the model was called', (t) => {
  const modelMock = require('./model.mock');

  modelMock.exec.resolves();
  modelMock.findOne.resolves();
  modelMock.lean.resolves();

  const BaseRepository = require('../template.class');
  const baseRepository = new BaseRepository(modelMock);

  t.deepEqual(baseRepository.model, modelMock);
  t.falsy(modelMock.exec.called);
  t.falsy(modelMock.findOne.called);
  t.falsy(modelMock.lean.called);
});

test('get without options is calling find with the query', function * (t) {
  const modelMock = require('./model.mock');

  modelMock.exec.resolves();
  modelMock.findOne.resolves();
  modelMock.lean.returns(modelMock);
  modelMock.find.returns(modelMock);
  const getOneSpy = sinon.spy();

  const BaseRepository = require('../template.class');
  BaseRepository.getOne = getOneSpy;

  const baseRepository = new BaseRepository(modelMock);
  yield baseRepository.get({ test: 'testText' });
  t.falsy(getOneSpy.calledOnce);
  t.truthy(modelMock.find.calledWithExactly({ test: 'testText' }));
});

test('get with limit = 1 is calling getOne with the query', function * (t) {
  const modelMock = require('./model.mock');

  modelMock.exec.resolves();
  modelMock.findOne.returns(modelMock);
  modelMock.lean.returns(modelMock);
  modelMock.find.returns(modelMock);

  const BaseRepository = require('../template.class');
  const baseRepository = new BaseRepository(modelMock);
  const getOneSpy = sinon.spy(baseRepository, 'getOne');

  yield baseRepository.get({ test: 'testText' }, { limit: 1 });
  t.truthy(getOneSpy.calledOnce);
  t.falsy(modelMock.find.called);
  t.truthy(modelMock.findOne.calledOnce);
  t.truthy(modelMock.lean.calledOnce);
});

test('create creates a model with the params and call create', function * (t) {
  const modelMock = require('./model.mock');
  modelMock.exec.resolves();
  modelMock.findOne.returns(modelMock);
  modelMock.lean.returns(modelMock);
  modelMock.find.returns(modelMock);
  modelMock.create.resolves();

  const BaseRepository = require('../template.class');
  const baseRepository = new BaseRepository(modelMock);

  yield baseRepository.create({ test: 'testText' });
  t.falsy(modelMock.exec.called);
  t.falsy(modelMock.lean.called);
  t.falsy(modelMock.find.called);
  t.falsy(modelMock.findOne.called);
  t.truthy(modelMock.create.calledOnce);
  t.truthy(modelMock.create.calledWithExactly({ test: 'testText' }));
});
