class RepositoryTemplate {
  constructor(model) {
    this.model = model;
  }

  * get(query, options = { }) {
    const { limit } = options;
    if (limit === 1) {
      return yield this.getOne(query);
    }

    const model = this.model.find(query);
    return yield model.lean().exec();
  }

  * getOne(query) {
    const model = this.model.findOne(query);
    return yield model.lean().exec();
  }

  create(data) {
    return this.model.create(data);
  }
}

module.exports = RepositoryTemplate;
