# express-boilerplate
This is the base project to quickly initialize other projects based on node.js + express + mongodb + redis.

It's thought like an API REST project like the major part of the nowadays projects but you can quickly add some template engine and adjust at your like.

The deploy system is based on Docke and the test suite is based on mockery, sinon and ava. This boilerplate has a 100% test coverage.

# Scripts
```javascript
"scripts": {
  "start": "node app.js",
  "test": "xo && NODE_ENV=test ava --verbose",
  "test:watch": "NODE_ENV=test ava --verbose -w",
  "test:cobertura": "NODE_ENV=test nyc ava",
  "test:ci": "NODE_ENV=test nyc --reporter=cobertura ava --tap | tap-xunit > express-boilerplate-report.xml",
  "lint": "xo",
  "lint:ci": "xo --reporter=checkstyle > checkstyle.xml"
}
```
Here we have the list of the scripts that we can run by default.
